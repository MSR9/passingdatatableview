//
//  ProductDetailViewController.swift
//  DataPassingTableView
//
//  Created by PrahladReddy on 2/12/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

class ProductDetailViewController: UITableViewController {
    
      // Model
    var product: Product?
    
    @IBOutlet weak var productImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        productImageView.image = product?.image
    }
}
