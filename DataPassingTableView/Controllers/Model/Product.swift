//
//  Product.swift
//  DataPassingTableView
//
//  Created by PrahladReddy on 2/12/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import Foundation
import UIKit

public enum ProductRating {
    case unrated
    case average
    case ok
    case good
    case brilliant
}

class Product {
    var title : String
    var description : String
    var image : UIImage
    var rating : ProductRating
    
    init(titled: String, descript: String, imageName: String) {
        
        title = titled
        description = descript
        
        if let img = UIImage(named: imageName) {
          image = img
        } else {
            image = UIImage(named: "default")!
        }
        rating = .unrated
    }
}
