//
//  TableViewCell.swift
//  DataPassingTableView
//
//  Created by PrahladReddy on 2/12/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productDescriptionlabel: UILabel!
    
    func configureCellWith(_ product: Product) {
        self.productImageView.image = product.image
        self.productTitleLabel.text = product.title
        self.productDescriptionlabel.text = product.description
    }
}
