//
//  AppleProductsTableViewController.swift
//  DataPassingTableView
//
//  Created by PrahladReddy on 2/12/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

class AppleProductsTableViewController: UITableViewController {

    lazy var productLines: [ProductLine] = { return ProductLine.productLines() }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = editButtonItem
        
        //Make the row height dynamic
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }

    // MARK: - UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let productLine = productLines[section]
        return productLine.name
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return productLines.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let productLine = productLines[section]
        return productLine.products.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Product Cell", for: indexPath) as! ProductTableViewCell
        
        let productLine = productLines[indexPath.section]
        let product = productLine.products[indexPath.row]
        
        cell.configureCellWith(product)
        return cell
    }
    
    // MARK: - Edit TableView
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            let productLine = productLines[indexPath.section]
            productLine.products.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
    }
    
    // MARK: - Moving Cells
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let productToMove = productLines[sourceIndexPath.section].products[sourceIndexPath.row]
        
        //Move targetedProduct to the Products
        productLines[destinationIndexPath.section].products.insert(productToMove, at: destinationIndexPath.row)
        
        //Delete the targetedProduct to fromProducts
        productLines[sourceIndexPath.section].products.remove(at: sourceIndexPath.row)
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            switch identifier {
            case "Show Detail":
                let productDetailVC = segue.destination as! ProductDetailViewController
                if let indexPath = self.tableView.indexPath(for: sender as! UITableViewCell) {
                    productDetailVC.product = productAtIndexPath(indexPath)
                }
            case "Show Edit":
                let editTableVC = segue.destination as! EditTableViewController
                if let indexPath = self.tableView.indexPath(for: sender as! UITableViewCell) {
                    editTableVC.product = productAtIndexPath(indexPath)
                }
            default: break
            }
        }
    }
    
    // MARK: - Helper Method
    func productAtIndexPath(_ indexPath: IndexPath) -> Product {
        let productLine = productLines[indexPath.section]
        return productLine.products[indexPath.row]
    }
}

